export const FILTER_BY_OPTIONS = {
  first: {
    label: "First Name",
    value: "first"
  },
  last: {
    label: "Last Name",
    value: "last"
  }
};
