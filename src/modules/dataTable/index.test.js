import React from 'react';
import { shallow } from 'enzyme';
//import renderer from 'react-test-renderer';
import DataTable from './';
import '../../setupTests';
import data from "../../data/names.json";
import { FILTER_BY_OPTIONS } from "../../constants";

describe ('dataTable', () => {
    let component = null;
    let tabledata = null;

    it ('renders correctly', () => {
        component = shallow(<DataTable />);
    });

    it ('matches snapshot', () => {
        expect(component).toMatchSnapshot();
    });

});
