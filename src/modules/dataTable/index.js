import React, { Component } from "react";

// components
import AppSelect from "../../components/appSelect";
import AppInput from "../../components/appInput";
import AppTable from "../../components/appTable";

// constants
import { FILTER_BY_OPTIONS } from "../../constants";

// styles
import styles from "./dataTableStyles.js";

// mockedData
import data from "../../data/names.json";

class DataTable extends Component {
  state = {
    filterBy: FILTER_BY_OPTIONS.first,
    inputValue: "",
    tableData: data
  };

  onSelectChange = (e, data) => {
    e.preventDefault();
    e.stopPropagation();
    this.setState(
      {
        filterBy: FILTER_BY_OPTIONS[data.value]
      },
      () => this.getTableData()
    );
  };

  onInputChange = e => {
    this.setState(
      {
        inputValue: e.target.value.trim().toLowerCase()
      },
      () => this.getTableData()
    );
  };

  getTableData() {
    const filterBy = this.state.filterBy.value;
    const filterValue = this.state.inputValue;
    let tableData = this.state.tableData;
    if (filterValue.length) {
      tableData = data.filter(item =>
        item[filterBy].toLowerCase().includes(filterValue)
      );
    } else {
      tableData = data;
    }

    this.setState({
      tableData
    });
  }

  render() {
    const { inputValue, tableData, filterBy } = this.state;
    return (
      <div>
        <div style={styles.tableHeader}>
          <AppSelect
            selected={filterBy}
            options={FILTER_BY_OPTIONS}
            onSelectChange={this.onSelectChange}
          />
          <AppInput
            onInputChange={this.onInputChange}
            inputValue={inputValue}
          />
        </div>
        <AppTable options={FILTER_BY_OPTIONS} tableData={tableData} />
      </div>
    );
  }
}

export default DataTable;
