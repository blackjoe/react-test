import React from "react";

const AppInput = ({ onInputChange, inputValue }) => {
  return (
    <div className="input-group">
      <input
        type="text"
        className="form-control"
        placeholder="Enter Value"
        aria-describedby="basic-addon2"
        value={inputValue}
        onChange={onInputChange}
      />
    </div>
  );
};

export default AppInput;
