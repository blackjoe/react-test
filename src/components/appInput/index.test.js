import React from 'react';
import { shallow } from 'enzyme';
import AppInput from './';
import '../../setupTests';

describe ('AppInput', () => {
    let component = null;
    let changed = null;

    const onInputChange = ( e ) => {
        changed = e.target.value.trim().toLowerCase();
    }

    it ('renders correctly', () => {
        component = shallow(<AppInput onInputChange={onInputChange} inputValue={'John'}/>);
    }); 

    it ('matches snapshot', () => {
        expect(component).toMatchSnapshot();
    }); 

    describe('input test', () => {
        it('has a input form', () => {
            expect(component.find('input').exists()).toBe(true);
        })
        it('simulates input change', () => {
            const mockedEvent = {
                target: {
                    value: 'Nick'
                }
            };
            component.find('input').simulate('change',mockedEvent);
            expect(changed).toBe('nick');
        })
    });
});
