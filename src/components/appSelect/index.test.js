import React from 'react';
import '../../setupTests';
import { shallow } from 'enzyme';
import AppSelect from './';
import {FILTER_BY_OPTIONS} from "../../constants";

describe ('AppSelect', () => {
    let component = null;
    let changed = null;

    const onChange = ( e, data ) => {
        changed = FILTER_BY_OPTIONS[data.value];
    }

    it ('renders correctly', () => {
        component = shallow(<AppSelect selected={FILTER_BY_OPTIONS.first} options={FILTER_BY_OPTIONS} onSelectChange={onChange}/>);
    }); 

    it ('matches snapshot', () => {
        expect(component).toMatchSnapshot();
    }); 

    describe('select test', () => {
        it('has a select form', () => {
            expect(component.find('button').exists()).toBe(true);
        })

/*
        it('simulates select change', () => {
            const mockedEvent = {
                target: {
                    value: FILTER_BY_OPTIONS.last
                }
            };
            component.find('button').simulate('change',mockedEvent);
            expect(changed).toBe(FILTER_BY_OPTIONS.last);
        })
*/
    });
});
