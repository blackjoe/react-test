import React, { useState } from "react";

const AppSelect = ({ options, onSelectChange, selected }) => {
  const [open, setOpen] = useState(false);

  const handleClick = (e, data) => {
    onSelectChange(e, data);
    setOpen(!open);
  };

  return (
    <React.Fragment>
      
      <div className={`dropdown ${open ? "open" : ""}`}>
        <label htmlFor="appSelect">Filter By</label>
        <button
          className="btn btn-default dropdown-toggle"
          type="button"
          id="appSelect"
          onClick={() => setOpen(!open)}
        >
         
          {selected ? selected.label : "Select Here"}
          <span className="caret"></span>
        </button>
        <ul className="dropdown-menu">
          {Object.keys(options).map(key => {
            return (
              <li
                key={`app-select-${options[key].value}`}
                value={options[key].value}
              >
                <a onClick={e => handleClick(e, options[key])}>
                  {options[key].label}
                </a>
              </li>
            );
          })}
        </ul>
      </div>
    </React.Fragment>
  );
};

export default AppSelect;
