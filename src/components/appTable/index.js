import React from "react";
// mockedData
import data from "../../data/names.json";

const AppTable = ({ options, tableData }) => {
  return (
    <table border="1" className="table">
      <tbody>
        <tr>
          {Object.keys(options).map(key => {
            return (
              <th key={`app-select-${options[key].value}`}>
                {options[key].label}
              </th>
            );
          })}
        </tr>
        {tableData.length ? (
          tableData.map(item => {
            return (
              <tr key={`${item.first}-${item.last}`}>
                <td>{item.first}</td>
                <td>{item.last}</td>
              </tr>
            );
          })
        ) : (
          <tr>
            <td>no data</td>
            <td>no data</td>
          </tr>
        )}
      </tbody>
    </table>
  );
};

export default AppTable;
