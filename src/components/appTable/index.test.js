import React from 'react';
import { shallow } from 'enzyme';
import AppTable from './';
import '../../setupTests';
import { FILTER_BY_OPTIONS } from "../../constants";
import data from "../../data/names.json";

describe ('AppTable', () => {
    let component = null;
    let changed = null;

    it ('renders correctly', () => {
        component = shallow(<AppTable options={FILTER_BY_OPTIONS} tableData={data}/>);
    }); 

    it ('matches snapshot', () => {
        expect(component).toMatchSnapshot();
    }); 

    describe('table test', () => {
        it('has a table form', () => {
            expect(component.find('table').exists()).toBe(true);
        })
    });
});
