import React from 'react';
import ReactDOM from 'react-dom';
import DataTable from "./modules/dataTable"

import './index.css';

const app = (
  <div>
    <h1>React Test Application</h1>
    <DataTable />
  </div>
);

ReactDOM.render(app, document.getElementById('root'));